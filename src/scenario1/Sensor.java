package scenario1;

import java.util.Random;

public class Sensor {
    public static final double OFFSET = 16;

    public double popNextPressurePsiValue(){
        double pressureTelemetryValue;
        pressureTelemetryValue = samplePressure();
        return OFFSET + pressureTelemetryValue;
    }

    //Violate single responsibility, Sensor generate samples which has nothing to do with the Sensor itself. Create SampleGenerator class
    private double samplePressure() {
        Random basicRandomNumbersGenerator = new Random();
        double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble();
        return pressureTelemetryValue;
    }
}
