package scenario1;

public class Alarm {
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    //Violation of Dependency inversion, should rather reference an interface
    //Violation of Open/Closed, can not add more sensors without adjusting the Alarm class
    Sensor sensor = new Sensor();
    boolean alarmOn = false; //field should be private

    public void check(){
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(){
        return alarmOn;
    }
}