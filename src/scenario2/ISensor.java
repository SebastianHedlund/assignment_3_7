package scenario2;

public interface ISensor {
    double popNextPressurePsiValue();
}
