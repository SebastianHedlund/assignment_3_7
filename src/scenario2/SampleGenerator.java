package scenario2;

import java.util.Random;

public class SampleGenerator implements ISampleGenerator {

    @Override
    public double samplePressure() {
        Random basicRandomNumbersGenerator = new Random();
        double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble();
        return pressureTelemetryValue;
    }

}
