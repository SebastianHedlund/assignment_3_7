package scenario2;

public class Sensor implements ISensor {
    public static final double OFFSET = 16;
    private ISampleGenerator generator;

    public Sensor(ISampleGenerator generator) {
        this.generator = generator;
    }

    @Override
    public double popNextPressurePsiValue(){
        double pressureTelemetryValue;
        pressureTelemetryValue = generator.samplePressure();
        return OFFSET + pressureTelemetryValue;
    }

}
