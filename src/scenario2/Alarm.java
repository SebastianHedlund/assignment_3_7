package scenario2;

public class Alarm {
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;
    private boolean alarmOn = false;
    private ISensor sensor;

    public Alarm(ISensor sensor) {
        this.sensor = sensor;
    }

    public void check(){
        double psiPressureValue = sensor.popNextPressurePsiValue();
        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    //Getters and Setters
    public boolean isAlarmOn(){
        return alarmOn;
    }
    public void setSensor(ISensor sensor) {
        this.sensor = sensor;
    }

}
